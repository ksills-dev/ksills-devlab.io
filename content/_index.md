+++
title = "Hello, Employers!"
+++

# Kenneth E. Sills
<div style="text-align:center">
  <a href="mailto: Kenneth.E.Sills@Gmail.com">Kenneth.E.Sills@Gmail.com </a> <br>
  <a href="mailto: KSills.Dev@Gmail.com">     KSills-Dev@Gmail.com      </a> <br>
  (727) 709-0761
</div>


<div style="text-align:center">
  <h2>Education</h2>

  <h4>University of South Florida</h4>
  Bachelors of Science - Computer Science

  <h4>Saint Petersburg College</h4>
    Early College - Associate of Arts
</div>

---

<table>
  <colgroup>
    <col style="width: 33%" />
    <col style="width: 33%" />
    <col style="width: 33%" />
  </colgroup>
<tr align="center">
  <td> <h3>Skills</h3>             <td> <h3>Languages</h3> <td> <h3>Tools</h3>
<tr align="center">
  <td> Native Development          <td> C++ ('95 → '17)      <td> GCC & Clang
<tr align="center">
  <td> Networking (UDP & TCP | IP) <td> C ('95 → '11)     <td> GDB & LLDB
<tr align="center">
  <td> Computer Security           <td> Rust (2018)        <td> CMake & Make
<tr align="center">
  <td> Qt Development Framework    <td> Ada (2012)         <td> Git
<tr align="center">
  <td> Linux / POSIX Development   <td> D (2.064)          <td> FLex & Bison
<tr align="center">
  <td rowspan=7>                   <td> Python 3
<tr align="center">
                                   <td> Lua (5.1 → 5.3)
<tr align="center">
                                   <td> Bash / Fish
<tr align="center">
                                   <td> LaTeX & Markup
<tr align="center">
                                   <td> VHDL on FPGA
<tr align="center">
                                   <td> HTML
</table>
