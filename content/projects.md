# Personal Works

<table>
<tr>
  <th> Name </th>
  <th> Description </th>
  <th> Lessons Learned </th>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/sk-chat">
      <b>SK-Chat</b>
    </a>
  </td>
  <td>
    The final project for USF's Network Security course. my team was tasked with
    producing a GUI application providing "secure" P2P text chat leveraging
    DES 56-bit encryption and a rotating key system. <br>
    In this project I utilize Qt5 Widgets, Verdigris (a replacement to the
    Qt MOC), the CMake build chain, and the Crypto++ library to build a
    non-trivial user facing tool.
  </td>
  <td>
    <ul>
      <li> The design process for achieving application security.
      <li> Making the best of a bad situation (forced to use DES-56).
      <li> Modern CMake dependency practices.
      <li> QT5 application development in C++.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/cnt-4004-project">
      <b>PROTO</b>
    </a>
  </td>
  <td>
    The final project for USF's Computer Networking course. A partner and I were
    responsible for creating a new communication protocol over UDP for the
    transfer of files of known length on a network of unknown bandwidth and
    reliability. The resulting product exceeded our expectations, providing
    to-the-bit reliability for over 250 Gigabit hours, convergence to optimal
    network saturation, congestion avoidance, and a 0.61 Jain TCP fairness
    index.
    <br>
    All projects were pitted against each other (and a reference TCP
    implementation) for performance and reliability - where our project
    received first place.
  </td>
  <td>
    <ul>
      <li> Designing a usable, flexible protocol under several constraints and
           fully utilizing assumptions is <i>hard</i>. Spend extra time in
           the design phase!
      <li> Bolstered my researching and "Google-Fu" skills looking through
           the extensive existing knowledge.
      <li> Debugging bitwise errors in a network environment.
      <li> Using network monitoring for algorithmic analysis.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/dj2dism">
      <b>Dj2Dism</b>
    </a>
  </td>
  <td>
    Over the course of my Compilers course, we individually developed a compiler
    in C for a provided language specification of "Diminished Java" (DJ).
    The compiler targetted an imaginary ISA called "Diminished ISM" (DISM). <br>
    I hestitate to include this publicly, due to the abysmal quality of the
    content. However, as it encompassed a not-insignificant amount of work and
    inspired me in future projects I include it with a note: <br>
    <i> The project
    as given had serious limitations. We were allowed no STL imports outside of
    `string.h`. We were not permitted to use multifile solutions. The professor
    provided header files that could not be modified in any way. </i>
  </td>
  <td>
    <ul>
      <li> Creating conflict-free lexing and parsing rules for a given language.
      <li> Using lexer and parser generators to feed a desired grammar AST to
           a compiler frontend.
      <li> Implementing type and semantic checking.
      <li> Implementing object generation for a given AST (including a single
           inheritance v-table).
      <li> How very differently I would structure a future project.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/cop-4600-project">
      <b>LWT</b>
    </a>
  </td>
  <td>
    The final project for OS Design - A lightweight parallel job queue
    implementation using POSIX pthreads and user contexts.
  </td>
  <td>
    <ul>
      <li> Debugging convoluted templated, platform-specific errors in
           native code.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/GrahamScan">
      <b>Graham</b>
    </a>
  </td>
  <td>
    An interactive Python application with TKInter that provides
    a step-by-step visualization and description of the Graham
    Scan hull generation algorithm.
  </td>
  <td>
    <ul>
      <li> Interactive GUI development using Python TKInter.
      <li> Take a partner's PoC, develop a flexible interface and a
           suitable interactive application.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/sterling">
      <b>Sterling Evaluator</b>
    </a>
  </td>
  <td>
    The final assignment for Programming Languages, a typechecker
    and evaluator for a custom ML variant "Sterling".
  </td>
  <td>
    <ul>
      <li> Formally verifying a programming language specification.
      <li> Practical application of the lambda (and pi) calculus.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/skul">
      <b>SKÜL</b>
    </a>
  </td>
  <td>
    Final Project for USF's Object Oriented Programming course - the creation of
    a simple school administration system in C++.
  </td>
  <td>
    <ul>
      <li> The importance of using Databases where appropriate!
      <li> How to build a large object graph with proper encapsulation and
           assignment of duties.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
       <a href="https://gitlab.com/ksills-dev/mstb">
           <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/7361791/Logo-circle-256.png" style="max-width:128px">
           <b>MSTB</b>
       </a>
  </td>
  <td>
    Markov's Simply Twitter Bot (MSTB) is a very simple Python script that
    scrapes the desired Twitter account history and generates text from a
    resulting Markov chain. It's usually nonsense, but it was fun to play
    with for a few minutes!<br>
    <b>NOTE:</b> Requires OAuth keys for the Twitter API - not included in the
    repository.
  </td>
  <td>
    <ul>
      <li> Using a real-world public access API.
      <li> Markov chains for great good.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/typotronic">
      <b>Typotronic</b>
    </a>
  </td>
  <td>
    The final project for my Analysis of Algorithms course, Typotronics is a
    C++ program designed to find a minimum-cost chain of common typos between an
    input and it's target output. The overall goal being to reduce the time
    complexity and absolute performance metrics of our algorithm.
  </td>
  <td>
    <ul>
      <li> Development of an algorithm from a requirement with no
           further guidance from an instructor.
      <li> Proper utilization of memoization to improve application performance.
      <li> Making performance / accuracy trade-offs.
      <li> Formal complexity analysis for a non-trivial algorithm.
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/Revisionist">
      <b>Revisionist</b>
    </a>
  </td>
  <td>
    A small Python script to extract data from Google Revision logs for my
    sister's school project.
  </td>
  <td>
    <ul>
      <li> Discovering and reverse engineering a third-party format for
           extraction.
      <li> Meeting client needs, even when the client isn't sure what they
           want!
    </ul>
  </td>
</tr>

<tr>
  <td style="text-align:center">
    <a href="https://gitlab.com/ksills-dev/ksills-dev.gitlab.io">
      <img src="https://gitlab.com/uploads/-/system/project/avatar/1179998/sandwhich-circle.png" style="max-width:128px">
      <b>This Website!</b>
    </a>
  </td>
  <td>
    This portfolio website is generated from Markdown via Hugo using the Learn
    theme, generously hosted by Gitlab Pages. Prior to this, I had not set up
    any significant webpage and had no real interaction with any front-end
    technology.
  </td>
  <td>
    <ul>
      <li> Setting up a static website with repository CI.
      <li> Brushing up my HTML.
    </ul>
  </td>
</tr>

</table>
